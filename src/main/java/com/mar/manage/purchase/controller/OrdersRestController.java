package com.mar.manage.purchase.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mar.manage.purchase.business.OrdersBusinnes;
import com.mar.manage.purchase.model.Orders;
import com.mar.manage.purchase.repository.OrdersRepository;



@RestController
@RequestMapping("/orders")
public class OrdersRestController {
	private static final Logger LOGGER = LoggerFactory.getLogger(OrdersRestController.class);
	protected OrdersRepository ordersRepository;
	protected OrdersBusinnes ordersBusinnes;
	
	
	@Autowired
	public OrdersRestController(OrdersRepository ordersRepository, OrdersBusinnes ordersBusinnes) {
		super();
		this.ordersRepository = ordersRepository;
		this.ordersBusinnes = ordersBusinnes;
	}
	
	
	@GetMapping("/")
	public ResponseEntity<List<Orders>> getAllOrders() {
		List<Orders> result = ordersRepository.findAll();
	
		
		return new ResponseEntity<>(
				result,
				HttpStatus.OK);
	}

	
	
	
	

}
