package com.mar.manage.purchase.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mar.manage.purchase.model.Orders;
import com.mat.manage.purchase.enume.State;

public interface OrdersRepository extends JpaRepository<Orders, Long>{
	
	public Optional<Orders> findById(Long id);
	

}
