package com.mat.manage.purchase.enume;

import java.util.Arrays;

public enum State {
   PANNIER(1,"Pannier"),
   ENCOURS(2,"En cours"),
   VALIDER(3,"Valider"),
   ENCOURDEPREPARATION(4,"En cours de preparation"),
   FINALISER(5,"Finalisé"),
   LIVRAISON(6,"Livraison"),
   LIVRER(7,"Livré");
	
   private final int id;
   private final String name;
   
   private State(int id, String name) {
	   this.id = id;
	   this.name = name;
   }
   
   public int getId() {
	   return id;
   }
   
   public String getName() {
	   return name;
   }
   
   public static State valueOf(int id){
				
		return Arrays.asList(State.values())
				.stream()
				.filter( x-> x.getId() == id)
				.findFirst()
				.orElse(null);
	}
	   
}
