CREATE TABLE Orders(
    id INT PRIMARY KEY NOT NULL IDENTITY(1,1),
	description VARCHAR(200) NULL,
	state VARCHAR(100) NULL,
    create_date DATEtime NULL,
    last_update_date DATEtime NULL,
);

